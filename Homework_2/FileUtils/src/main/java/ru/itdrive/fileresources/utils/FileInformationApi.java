package ru.itdrive.fileresources.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileInformationApi {

    public static List <FileInformation> getFileInformations(File dir1 ) {

        List <FileInformation> fileinfo = new ArrayList<>();
        for (File file : dir.listFiles()) {
            fileinfo.add(new FileInformation( file.getName() , file.length()));
        }
        return fileinfo;

    }
}
