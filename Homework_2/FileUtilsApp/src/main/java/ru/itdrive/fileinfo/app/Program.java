
package ru.itdrive.fileinfo.app;

import ru.itdrive.fileresources.utils.FileInformation;
import ru.itdrive.fileresources.utils.FileInformationApi;
import com.beust.jcommander.JCommander;
import java.io.File;
import java.util.List;


public class Program {

    public static void main(String[] args) {

      Arguments arguments = new Arguments();

      JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

       File dir = new File(arguments.path);
      
       List<FileInformation> fileInformations = new FileInformationApi().getFileInformations(dir);

        for (int i =0; i < fileInformations.size(); i++) {

            FileInformation fileinfo = fileInformations.get(i);
            System.out.println(fileinfo.fileName + " " + fileinfo.fileSize + " bytes");

        }
    }
}

