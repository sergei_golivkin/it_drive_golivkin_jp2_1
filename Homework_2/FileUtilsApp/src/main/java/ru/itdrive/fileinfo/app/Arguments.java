package ru.itdrive.fileinfo.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;


@Parameters(separators = "=")
class Arguments{

	@Parameter(names = {"--path1"})
	public String path;

	@Parameter(names = {"--path2"})
	public String path;

	@Parameter(names = {"--path3"})
	public String path;
}