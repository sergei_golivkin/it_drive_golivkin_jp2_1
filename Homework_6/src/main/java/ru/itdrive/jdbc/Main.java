package ru.itdrive.jdbc;

import ru.itdrive.jdbc.models.Course;
import ru.itdrive.jdbc.models.Lesson;
import ru.itdrive.jdbc.repositories.CoursesRepository;
import ru.itdrive.jdbc.repositories.LessonsRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class Main {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "123456";

    public static void main(String[] args) throws SQLException {

        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
        LessonsRepository lessonsRepository = new LessonsRepository(connection);

        Lesson lesson = lessonsRepository.find(3);
        System.out.println(lesson);

        Map<Long,Lesson> lessons = lessonsRepository.findAll();
        System.out.println(lessons);
        System.out.println("------------------------------------------------------------------");

        CoursesRepository coursesRepository = new CoursesRepository(connection);
        Course course = coursesRepository.find(2);
        System.out.println(course);

        Map<Long, Course> courses = coursesRepository.findAll();
        System.out.println(courses);


    }
}

