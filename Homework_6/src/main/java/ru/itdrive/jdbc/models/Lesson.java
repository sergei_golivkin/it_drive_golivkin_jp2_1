package ru.itdrive.jdbc.models;

public class Lesson {
    private Long id;
    private String name;
    private Long courseId;
    private Course course;

    public Lesson(Long id, String name, Long courseId, Course course) {
        this.id = id;
        this.name = name;
        this.courseId = courseId;
        this.course = course;


   }

    public Long getId() {
        return id ;
    }

    public String getName() {
        return name;
    }

    public Long getCourseId() {
        return courseId;
    }

    public Course getCourse() {
        return course;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", courseId=" + courseId +
                '}';
    }

}
