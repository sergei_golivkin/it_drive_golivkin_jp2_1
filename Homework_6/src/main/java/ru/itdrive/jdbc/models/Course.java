package ru.itdrive.jdbc.models;

import java.util.List;


public class Course {

    private long id;
    private String title;
    private List<Lesson> lessons;

    public Course() {

    }


    public long getId() {
        return id;
    }

    public Course(long id, String title, List<Lesson> lessons) {
        this.lessons = lessons;
        this.id = id;
        this.title = title;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", lessons=" + lessons +
                '}';
    }
}
