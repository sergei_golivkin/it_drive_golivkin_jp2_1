package ru.itdrive.jdbc.repositories;

import ru.itdrive.jdbc.models.Course;
import ru.itdrive.jdbc.models.Lesson;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class CoursesRepository implements CrudRepository<Course> {

    //language=SQL
    private static final String SQL_SELECT_BY_FIND = "select c.id as c_id, c.title as c_title, l.id as l_id, l.name as  l_name, l.course_id as l_course_id from course c left join lesson l on l.course_id = c.id where c.id = ";

    //language=SQL
    private static final String SQL_SELECT_BY_FINDALL = "select  * from course c left join lesson l on l.course_id = c.id ";

    private final Connection connection;


    private final RowMapper<Course> courseRowMapper = new RowMapper<Course>() {
        public Course mapRow(ResultSet resultSet) throws SQLException {

            List<Lesson> lessons = new ArrayList<Lesson>();

            return new Course(
                    resultSet.getLong("id"),
                    resultSet.getString("title"), lessons) {
            };
        }
    };

    public CoursesRepository(Connection connection) throws SQLException {
        this.connection = connection;
    }

    public Course find(Integer id) {
        try {

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_FIND + id);
            List<Lesson> lessons = new ArrayList<Lesson>();
            Course course = null;

            if (resultSet.next()) {
                course = new Course(resultSet.getLong("c_id"), resultSet.getString("c_title"), lessons);
                Lesson lesson = new Lesson(resultSet.getLong("l_id"), resultSet.getString("l_name"), resultSet.getLong("l_course_id"), course);
                lessons.add(lesson);
            } else return null;

            while (resultSet.next()) {
                Lesson lesson = new Lesson(resultSet.getLong("l_id"), resultSet.getString("l_name"), resultSet.getLong("l_course_id"), course);
                lessons.add(lesson);
            }

            return course;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Map<Long, Course> findAll() {
        try {
            Map<Long, Course> courses = new HashMap<Long, Course>();

            Statement statement = connection.createStatement();
            ResultSet row = statement.executeQuery(SQL_SELECT_BY_FINDALL);


            while (row.next()) {
                if (!courses.containsKey(row.getLong("id"))) {

                    Course newCourse = new Course(row.getLong("id"), row.getString("title"), new ArrayList<Lesson>());
                    courses.put(newCourse.getId(), newCourse);
                }

                Course existedCourse = courses.get(row.getLong("id"));

                existedCourse.getLessons().add(new Lesson(row.getLong("id"),
                        row.getString("name"), row.getLong("course_id"), existedCourse));
            }


            return courses;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

}