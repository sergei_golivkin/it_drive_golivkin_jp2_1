package ru.itdrive.jdbc.repositories;


import java.util.Map;

public interface CrudRepository<T> {

    T find(Integer id);

    Map<Long, T> findAll();

}
