package ru.itdrive.jdbc.repositories;

import ru.itdrive.jdbc.models.Course;
import ru.itdrive.jdbc.models.Lesson;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class LessonsRepository implements CrudRepository<Lesson> {

    //language=SQL
    private static final String SQL_SELECT_FINDALL = "select * from lesson l  left join course c  on l.course_id = c.id";

    //language=SQL
    private static final String SQL_SELECT_BY_FIND = "select * from lesson where id = ";

    private final Connection connection;


    private final RowMapper<Lesson> lessonRowMapper = new RowMapper<Lesson>() {
        public Lesson mapRow(ResultSet row) throws SQLException {
            Course course = new Course();
            return new Lesson(
                    row.getLong("id"),
                    row.getString("name"),
                    row.getLong("course_id"),
                    course
            );
        }
    };

    public LessonsRepository(Connection connection) {
        this.connection = connection;
    }

    public Lesson find(Integer id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_FIND + id);
            resultSet.next();
            return lessonRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    public Map<Long, Lesson> findAll() {
        try {
            Map<Long, Lesson> lesson = new HashMap<Long, Lesson>();
            Course course = new Course();
            Statement statement = connection.createStatement();
            ResultSet row = statement.executeQuery(SQL_SELECT_FINDALL);
            while (row.next()) {

                if (!lesson.containsKey(row.getLong("id"))) {

                    Lesson newLesson = new Lesson(row.getLong("id"), row.getString("name"),
                            row.getLong("course_id"), course);
                    lesson.put(newLesson.getId(), newLesson);
                }

            }
            return lesson;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }
}
