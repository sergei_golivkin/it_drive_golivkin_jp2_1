package ru.itdrive.chat.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ChatServer {

    private List<ConnectedClient> clients = new ArrayList<ConnectedClient>();

    class ConnectedClient extends Thread {

        private Socket server;
        private BufferedReader in;
        private BufferedWriter out;

        private ConnectedClient(Socket server) throws IOException {
            this.server = server;
            this.in = new BufferedReader(new InputStreamReader(server.getInputStream()));
            this.out = new BufferedWriter(new OutputStreamWriter(server.getOutputStream()));
        }

        private void onConnectionReady(ConnectedClient socketClient) {
            sendToAllConnections("Client connected: " + socketClient);

        }

        private void onReceiveString(ConnectedClient socketClient, String value) {
            sendToAllConnections(value);
        }

        private void onDisconnect(ConnectedClient socketClient) {
            clients.remove(socketClient);
            sendToAllConnections("Client disconnected: " + socketClient);

        }

        private void onException(ConnectedClient socketClient, Exception e) {
            System.out.println("MyConnection exception:" + e);
        }

        private void sendToAllConnections(String value) {
            System.out.println(value);
            for (ConnectedClient connection : clients) connection.send(value);
        }

        private  void sendToConnection(String value) {
         System.out.println(value);
        }


        public void run() {

            try {
                onConnectionReady(ConnectedClient.this);

                while (!this.isInterrupted()) {
                    onReceiveString(ConnectedClient.this, in.readLine());
                }
            } catch (IOException e) {
                onException(ConnectedClient.this, e);
            } finally {
                onDisconnect(ConnectedClient.this);

            }
        }


        private void send(String value) {
            try {
                out.write(value + "\r\n");
                out.flush();
            } catch (IOException e) {
                onException(ConnectedClient.this, e);
                disconnect();

            }

        }

        private void disconnect() {
            this.interrupt();
            try {
                server.close();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }


        @Override
        public String toString() {
            return "MyConnection: " + server.getInetAddress() + ": " + server.getPort();
        }

    }

    public void start(int port) {

        try {
            ServerSocket server = new ServerSocket(port);

            while (true) {
                Socket client = server.accept();
                ConnectedClient socketClient = new ConnectedClient(client);
                clients.add(socketClient);
                socketClient.start();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void menu(){

        System.out.println("Hi World");
    }

}
