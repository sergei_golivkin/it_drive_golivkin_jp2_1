package ru.itdrive.chat.repositories;

import ru.itdrive.chat.models.Message;

public interface MessageRepository extends CrudRepository<Message> {

}
