package ru.itdrive.chat.repositories;

public interface CrudRepository<T> {

    void save(T object);

    void update(T object);

    void delete(Integer id);

    T find();

    T findAll();

}
