package ru.itdrive.chat.client;

import java.util.Scanner;

public class MainForClient {
    public static void main(String[] args)  {

        String host = "127.0.0.1";
        int port = 7777;
        Scanner scanner = new Scanner(System.in);
        ChatClient client = new ChatClient(host, port);

        while (true) {
            String message = scanner.nextLine();
            client.sendMessage(message);

        }

    }
}
