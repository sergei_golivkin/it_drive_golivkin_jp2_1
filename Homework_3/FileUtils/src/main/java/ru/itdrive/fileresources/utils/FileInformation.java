package ru.itdrive.fileresources.utils;

public class FileInformation {
     public String fileName;
     public long fileSize;


    FileInformation(String fileName, long fileSize) {
        this.fileName = fileName;
        this.fileSize = fileSize;
    }
}   