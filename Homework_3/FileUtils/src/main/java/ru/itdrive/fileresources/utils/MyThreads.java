package ru.itdrive.fileresources.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyThreads extends Thread {
    private File mainFolder;

    public  MyThreads(File mainFolder){
        this.mainFolder = mainFolder;
    }

    @Override
    public void run() {
        Map<String, List<FileInformation>> files = new HashMap<>();

        if (mainFolder.listFiles() != null ){
            for (File currentFolder : mainFolder.listFiles() ){

                if (currentFolder.isDirectory()) {

                    files.put(currentFolder.getName(), new ArrayList<>());

                    if (currentFolder.listFiles() != null) {

                        for (File file : currentFolder.listFiles()) {

                            FileInformation fileInformation = new FileInformation(file.getName(), file.length());

                            List<FileInformation> currentFolderFiles = files.get(currentFolder.getName());

                            currentFolderFiles.add(fileInformation);
                        }
                    }
                }
            }
        }
        for (Map.Entry<String, List<FileInformation>> map : files.entrySet()) {
            System.out.println("Folder: " + map.getKey());
            List<FileInformation> list = map.getValue();
            for ( FileInformation info : list ) {
                System.out.println("File: " + info.fileName + ", size: " + info.fileSize + " bytes");
            }
        }
        System.out.println();
    }
}
