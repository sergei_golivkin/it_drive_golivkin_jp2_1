package ru.itdrive.fileinfo.app;

import ru.itdrive.fileresources.utils.FileInformation;
import com.beust.jcommander.JCommander;
import ru.itdrive.fileresources.utils.MyThreads;
import java.io.File;


public class Program  {

    public static void main(String[] args) {

      Arguments arguments = new Arguments();

      JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

         File dir = new File(arguments.path);
 
         MyThreads myThreads =  new MyThreads(dir);
         myThreads.start();

       
    
        
    }
}

