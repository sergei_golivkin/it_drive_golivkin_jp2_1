package ru.itdrive.client.app;

import com.beust.jcommander.JCommander;

import java.util.Scanner;

public class MyClient {
    public static void main(String[] args) {

        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);



        Scanner scanner = new Scanner(System.in);
        SocketClient client = new SocketClient(arguments.host, arguments.port);

        while (true){
            String message = scanner.nextLine();
            client.sendMessage(message);
        }
    }
}
