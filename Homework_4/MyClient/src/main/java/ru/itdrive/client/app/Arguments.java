package ru.itdrive.client.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
  public class Arguments{

    @Parameter(names = {"--serverHost"})
    public String host;

    @Parameter(names = {"--serverPort"})
     public int port;

}