package ru.itdrive.server.app;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;


public class MyConnectionServer {


    private List<SocketClient> clients = new ArrayList<SocketClient>();

    class SocketClient extends Thread {

        private Socket socket;
        private BufferedReader in;
        private BufferedWriter out;

        private SocketClient(Socket socket) throws IOException {
            this.socket = socket;
            this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        }

        private synchronized void onConnectionReady(SocketClient socketClient) {
            sendToAllConnections("Client connected: " + socketClient);
        }

        private synchronized void onReceiveString(SocketClient socketClient, String value) {
            sendToAllConnections(value);
        }

        private synchronized void onDisconnect(SocketClient socketClient) {
            clients.remove(socketClient);
            sendToAllConnections("Client disconnected: " + socketClient);

        }

        private synchronized void onException(SocketClient socketClient, Exception e) {
            System.out.println("MyConnection exception:" + e);
        }

        private synchronized void sendToAllConnections(String value) {
            System.out.println(value);
            for ( SocketClient connection : clients ) connection.send(value);
        }


        public void run() {

            try {
                onConnectionReady(SocketClient.this);

                while (!this.isInterrupted()) {
                    onReceiveString(SocketClient.this, in.readLine());
                }
            } catch (IOException e) {
                onException(SocketClient.this, e);
            } finally {
                onDisconnect(SocketClient.this);

            }
        }


        private synchronized void send(String value) {
            try {
                out.write(value + "\r\n");
                out.flush();
            } catch (IOException e) {
                onException(SocketClient.this, e);
                disconnect();

            }

        }

        private synchronized void disconnect() {
            this.interrupt();
            try {
                socket.close();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }


        @Override
        public String toString() {
            return "MyConnection: " + socket.getInetAddress() + ": " + socket.getPort();
        }

    }

    public void start(int port) {

        try {
            ServerSocket server = new ServerSocket(port);

            while (true) {
                Socket client = server.accept();
                SocketClient socketClient = new SocketClient(client);
                clients.add(socketClient);
                socketClient.start();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


}
