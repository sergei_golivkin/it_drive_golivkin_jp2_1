package ru.itdrive.server.app;

import com.beust.jcommander.JCommander;

import java.io.IOException;
import java.net.ServerSocket;

public class MyServer {
    public static void main(String[] args) {

        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        System.out.println("Server running...");

        MyConnectionServer server = new MyConnectionServer();
        server.start(arguments.port);




    }



    }
