create table "user"
(
    user_id    serial primary key,
    first_name char(20),
    last_name  char(20),
    mail       char(20),
    password   char(20)

);

create table chat
(
    chat_id          serial primary key,
    title            char(30),
    author_id        integer,
    foreign key (author_id) references "user" (user_id),
    date_of_creation timestamp
);

create table party
(
    chat_id integer,
    foreign key (chat_id) references chat (chat_id),
    user_id integer,
    foreign key (user_id) references "user" (user_id)
);

create table message
(
    message_id       serial primary key,
    chat_id          integer,
    foreign key (chat_id) references chat (chat_id),
    author_id        integer,
    foreign key (author_id) references "user" (user_id),
    content          char,
    date_of_creation timestamp,
    is_read          boolean
);



