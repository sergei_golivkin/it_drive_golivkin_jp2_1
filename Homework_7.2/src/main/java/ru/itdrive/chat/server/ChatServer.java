package ru.itdrive.chat.server;

import ru.itdrive.chat.client.ChatClient;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ChatServer {

    private List<ChatClient> clients = new ArrayList<ChatClient>();

    class ChatClient extends Thread {

        private Socket socket;
        private BufferedReader in;
        private BufferedWriter out;

        private ChatClient(Socket socket) throws IOException {
            this.socket = socket;
            this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        }

        private void onConnectionReady(ChatClient socketClient) {
            sendToAllConnections("Client connected: " + socketClient);

        }

        private void onReceiveString(ChatClient socketClient, String value) {
            sendToAllConnections(value);
        }

        private void onDisconnect(ChatClient socketClient) {
            clients.remove(socketClient);
            sendToAllConnections("Client disconnected: " + socketClient);

        }

        private void onException(ChatClient socketClient, Exception e) {
            System.out.println("MyConnection exception:" + e);
        }

        private void sendToAllConnections(String value) {
            System.out.println(value);
            for (ChatClient connection : clients) connection.send(value);
        }

        private  void sendToConnection(String value) {
            System.out.println(value);
        }


        public void run() {

            try {
                onConnectionReady(ChatClient.this);

                while (!this.isInterrupted()) {
                    onReceiveString(ChatClient.this, in.readLine());
                }
            } catch (IOException e) {
                onException(ChatClient.this, e);
            } finally {
                onDisconnect(ChatClient.this);

            }
        }


        private void send(String value) {
            try {
                out.write(value + "\r\n");
                out.flush();
            } catch (IOException e) {
                onException(ChatClient.this, e);
                disconnect();

            }

        }

        private void disconnect() {
            this.interrupt();
            try {
                socket.close();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }


        @Override
        public String toString() {
            return "MyConnection: " + socket.getInetAddress() + ": " + socket.getPort();
        }

    }

    public void start(int port) {

        try {
            ServerSocket server = new ServerSocket(port);

            while (true) {
                Socket client = server.accept();
                ChatClient socketClient = new ChatClient(client);
                clients.add(socketClient);
                socketClient.start();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
