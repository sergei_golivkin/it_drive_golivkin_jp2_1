package ru.itdrive.jdbc.statements.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data


public class Course {

    private long id;
    private String title;
    private List<Lesson> lessons;

}
