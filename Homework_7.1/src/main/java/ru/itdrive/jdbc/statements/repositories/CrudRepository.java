package ru.itdrive.jdbc.statements.repositories;

import java.util.Map;

public interface CrudRepository<T> {

    void save(T object);

    void update(T object);

    void delete(Integer id);

    T find(Integer id);

    Map<Long, T> findAll();

}
