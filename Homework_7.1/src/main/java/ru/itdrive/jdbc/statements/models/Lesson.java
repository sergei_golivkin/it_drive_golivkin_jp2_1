package ru.itdrive.jdbc.statements.models;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class Lesson {

    private Long id;
    private String name;
    private Long courseId;

    @ToString.Exclude
    private Course course;


}
