package ru.itdrive.jdbc.statements.repositories;

import ru.itdrive.jdbc.statements.models.Course;
import ru.itdrive.jdbc.statements.models.Lesson;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class LessonsRepository implements CrudRepository<Lesson> {

    //language=SQL
    private static final String SQL_SELECT_BY_LESSON_FIND_ALL = "select * from lesson l  left join course c  on l.course_id = c.id";

    //language=SQL
    private static final String SQL_SELECT_BY_LESSON_FIND = "select * from lesson where id = ?";

    private final Connection connection;


    private final RowMapper<Lesson> lessonRowMapper = new RowMapper<Lesson>() {
        public Lesson mapRow(ResultSet row) throws SQLException {
            Course course = new Course();
            return new Lesson(
                    row.getLong("id"),
                    row.getString("name"),
                    row.getLong("course_id"),
                    course
            );
        }
    };

    public LessonsRepository(Connection connection) {
        this.connection = connection;
    }

    public void save(Lesson object) {

    }

    public void update(Lesson object) {

    }

    public void delete(Integer id) {

    }

    public Lesson find(Integer id) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_LESSON_FIND);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();

            return lessonRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Map<Long, Lesson> findAll() {
        try {
            Map<Long, Lesson> lesson = new HashMap<Long, Lesson>();
            Course course = new Course();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_LESSON_FIND_ALL);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                if (!lesson.containsKey(resultSet.getLong("id"))) {

                    Lesson newLesson = new Lesson(resultSet.getLong("id"), resultSet.getString("name"),
                            resultSet.getLong("course_id"), course);
                    lesson.put(newLesson.getId(), newLesson);
                }

            }

            preparedStatement.close();
            return lesson;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

}
