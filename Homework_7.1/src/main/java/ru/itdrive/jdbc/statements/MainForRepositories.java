package ru.itdrive.jdbc.statements;

import ru.itdrive.jdbc.statements.models.Course;
import ru.itdrive.jdbc.statements.repositories.CoursesRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;

public class MainForRepositories {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "123456";

    public static void main(String[] args) throws Exception {

        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        CoursesRepository coursesRepository = new CoursesRepository(connection);

        Scanner scanner = new Scanner(System.in);
        String title = scanner.nextLine();

        Course course = Course.builder()
                .title(title)
                .build();
        coursesRepository.save(course);
    }
}
