package ru.itdrive.jdbc.statements.repositories;

import ru.itdrive.jdbc.statements.models.Course;
import ru.itdrive.jdbc.statements.models.Lesson;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoursesRepository implements CrudRepository<Course> {

    //language=SQL
    private static final String SQL_SELECT_BY_COURSE_FIND = "select c.id as c_id, c.title as c_title, l.id as l_id, l.name as  l_name, l.course_id as l_course_id from course c left join lesson l on l.course_id = c.id where c.id = ? ";

    //language=SQL
    private static final String SQL_SELECT_BY_COURSE_FIND_ALL = "select  * from course c left join lesson l on l.course_id = c.id ";

    //language=SQL
    private static final String SQL_INSERT_COURSE = "  insert into course(title) values (?)";

    private final Connection connection;


    private final RowMapper<Course> courseRowMapper = new RowMapper<Course>() {
        public Course mapRow(ResultSet resultSet) throws SQLException {

            List<Lesson> lessons = new ArrayList<Lesson>();

            return new Course(
                    resultSet.getLong("id"),
                    resultSet.getString("title"), lessons) {
            };
        }
    };

    public CoursesRepository(Connection connection) throws SQLException {
        this.connection = connection;
    }

    public void save(Course object) {

        try {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_COURSE);
            statement.setString(1, object.getTitle());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    public void update(Course object) {

    }

    public void delete(Integer id) {

    }

    public Course find(Integer id) {
        try {

            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_COURSE_FIND);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Lesson> lessons = new ArrayList<Lesson>();
            Course course = null;

            if (resultSet.next()) {
                course = new Course(resultSet.getLong("c_id"), resultSet.getString("c_title"), lessons);
                Lesson lesson = new Lesson(resultSet.getLong("l_id"), resultSet.getString("l_name"), resultSet.getLong("l_course_id"), course);
                lessons.add(lesson);
            } else return null;

            while (resultSet.next()) {
                Lesson lesson = new Lesson(resultSet.getLong("l_id"), resultSet.getString("l_name"), resultSet.getLong("l_course_id"), course);
                lessons.add(lesson);
            }
            preparedStatement.close();

            return course;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Map<Long, Course> findAll() {
        try {
            Map<Long, Course> courses = new HashMap<Long, Course>();

            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_COURSE_FIND_ALL);
            ResultSet row = preparedStatement.executeQuery();


            while (row.next()) {
                if (!courses.containsKey(row.getLong("id"))) {

                    Course newCourse = new Course(row.getLong("id"), row.getString("title"), new ArrayList<Lesson>());
                    courses.put(newCourse.getId(), newCourse);
                }

                Course existedCourse = courses.get(row.getLong("id"));

                existedCourse.getLessons().add(new Lesson(row.getLong("id"),
                        row.getString("name"), row.getLong("course_id"), existedCourse));
            }
            preparedStatement.close();

            return courses;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

}
