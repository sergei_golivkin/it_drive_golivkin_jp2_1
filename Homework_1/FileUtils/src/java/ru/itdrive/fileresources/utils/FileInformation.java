package ru.itdrive.fileresources.utils;

public class FileInformation {
     String fileName;
     long fileSize;


    FileInformation(String fileName, long fileSize) {
        this.fileName = fileName;
        this.fileSize = fileSize;
    }
}   